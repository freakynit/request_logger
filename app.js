var PORT = 9999;

var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser()); 
app.all("*", function(req, res, next){
	console.log("===================== New request =====================");
	console.log("Body: " + JSON.stringify(req.body));
	console.log("Query: " + JSON.stringify(req.query));
	console.log("Params: " + JSON.stringify(req.params));
	console.log("Headers: " + JSON.stringify(req.headers));
	console.log("\n\n");
	
	res.json({"status": "success"});
});

app.listen(PORT);
